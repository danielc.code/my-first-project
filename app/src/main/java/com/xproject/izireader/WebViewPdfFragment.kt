package com.xproject.izireader

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_web_view_pdf.*

class WebViewPdfFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_web_view_pdf, container, false)

        return view
    }


    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            WebViewPdfFragment().apply {
                arguments = Bundle().apply {
                }
            }
    }
}
